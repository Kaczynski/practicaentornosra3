package principal;

import java.util.Scanner;
/**
 * 
 * @author lukas
 */
public class Generador {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);                               //Aqui faltaria el try catch parael manejo de errores

        menu();                                                                 //Creado el metodo menu
        int longitud = scanner.nextInt();

        System.out.println("Elige tipo de password: ");
        int opcion = scanner.nextInt();

        String password = "";
        for (int i = 0; i < longitud; i++) {                                    //El for engloba dodo el switch lo que hace inncesario su repeticion en cada caso
            switch (opcion) {
                case 1:
                    password = letras(password);                                //Creado el metodo letras
                    break;
                case 2:
                    password = numeros(password);                               //Creado el metodo numeros
                    break;
                case 3:
                    password = numerica(password);                              //Creado el metodo numerica
                    break;
                case 4:
                    password = alfanumerica(password);                          //Creado el metodo alfanumerica
                    break;
            }
        }
        System.out.println(password);
        scanner.close();
    }

    private static void menu() {
        System.out.println("Programa que genera passwords de la longitud "      //Sepracion de la cadena para que no se amuy larga
                + "indicada, y del rango de caracteres");
        System.out.println("1 - Caracteres desde A - Z");
        System.out.println("2 - Numeros del 0 al 9");
        System.out.println("3 - Letras y caracteres especiales");
        System.out.println("4 - Letras, numeros y caracteres especiales");
        System.out.println("Introduce la longitud de la cadena: ");
    }

    private static String alfanumerica(String password) {
        int n;
        n = (int) (Math.random() * 3);
        switch (n) {
            case 1:
                password = letras(password);                                    //Creado el metodo letras
                break;
            case 2:
                password = caracteres(password);                                //Creado el metodo catacteres
                break;
            default:
                password = numeros(password);                                   //Creado el metodo numeros
                break;
        }
        return password;
    }

    private static String numerica(String password) {
        int n;
        n = (int) (Math.random() * 2);
        if (n == 1) {
            password = letras(password);
        } else {
            password = caracteres(password);
        }
        return password;
    }

    private static String caracteres(String password) {
        char caracter4;
        caracter4 = (char) ((Math.random() * 15) + 33);
        password += caracter4;
        return password;
    }

    private static String numeros(String password) {
        int numero2;
        numero2 = (int) (Math.random() * 10);
        password += numero2;
        return password;
    }

    private static String letras(String password) {
        char letra1;
        letra1 = (char) ((Math.random() * 26) + 65);
        password += letra1;
        return password;
    }

}
