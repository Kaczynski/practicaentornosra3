import java.util.Scanner;

/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */
/**
 *
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactorizar1 {                                                    //Cambiado el nombre acorde con el nombre del fichero.

    final static String SALUDO = "Bienvenido al programa";                      //Cambiado el nombre acorde con el cometido y tipo
    private static final int NUMBER_OF_DAYS = 7;                                //Cambiado el nombre acorde con el cometido y declarado tipo de variable acorde a su uso 
    private static final int MAGIC_NUMBER = 16;                                 //Cambiado el nombre acorde con el cometido y declarado tipo de variable acorde a su uso

    private static final String DIAS[] = new String[]{
        "Lunes",
        "Martes",
        "Miercoles",
        "Jueves",
        "Viernes",
        "Sabado",
        "Domingo"
    };                                                                          //Cambiado el nombre acorde con el cometido y declarado tipo de variable acorde a su uso

    public static void main(String[] args) {
        Scanner c = new Scanner(System.in);

        System.out.println(SALUDO + "\nIntroduce tu dni");
        String dni = c.nextLine();                                              //Cambiado el nombre acorde con el cometido y declaracion de la misma acorde a su ambito
        System.out.println("Introduce tu nombre");
        String nombre = c.nextLine();                                           //Cambiado el nombre acorde con el cometido y declaracion de la misma acorde a su ambito

        int operador = 25;                                                      //Cambiado el nombre acorde con el cometido y declaracion de la misma acorde a su ambito
        if ((NUMBER_OF_DAYS > MAGIC_NUMBER)
                || (operador % 5 != 0)
                && ((operador * 3 - 1) > MAGIC_NUMBER / operador)) {            //Separacion del codigo para que no sea muy "largo"
            System.out.println("Se cumple la condición");
        }

        operador = NUMBER_OF_DAYS + MAGIC_NUMBER * operador
                + MAGIC_NUMBER / NUMBER_OF_DAYS;                                //Separacion del codigo para que no sea muy "largo"

        recorrer_array();
    }

    static void recorrer_array() {
        for (int i = 0; i < NUMBER_OF_DAYS; i++) {                              //Cambio del numero 7 por la constante de deias
            System.out.println("El dia de la semana en el que te encuentras ["
                    + (i + 1) + "-7] es el dia: " + DIAS[i]);                   //Separacion del codigo para que no sea muy "largo"
        }
    }

}
