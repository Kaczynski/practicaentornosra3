/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

import java.util.Scanner;

/**
 *
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactorizar2 {                                                    //Cambiado el nombre acorde con el nombre del fichero.

    static Scanner sc = new Scanner(System.in);                                 //Declaracion e inicializacion en la mismalinea
    private static final int CANTIDAD_MAX_ALUMNOS = 10;                         //Cambiado el nombre acorde con el cometido y declarado tipo de variable acorde a su uso

    public static void main(String[] args) {

        int alumnos[] = new int[CANTIDAD_MAX_ALUMNOS];                          //Cambiado el nombre acorde con el cometido y sustitucion por constante
        for (int i = 0; i < CANTIDAD_MAX_ALUMNOS; i++) {                        //Cambiado el nombre acorde con el cometido.
            System.out.println("Introduce nota media de alumno");
            alumnos[i] = sc.nextInt();
        }
        sc.close();
        System.out.println("El resultado es: " + recorrer_array(alumnos));
    }

    static double recorrer_array(int alumnos[]) {
        double total = 0;                                                       //Cambiado el nombre acorde con el cometido 
        for (int i = 0; i < CANTIDAD_MAX_ALUMNOS; i++) {                        //Cambiado el nombre acorde con el cometido y sustitucion por constante
            total += alumnos[i];
        }
        return total / 10;
    }

}
