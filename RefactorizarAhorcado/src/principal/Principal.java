package principal;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Principal {

    private final static byte NUM_PALABRAS = 20;
    private final static byte FALLOS = 7;
    private static final String[] PALABRAS = new String[NUM_PALABRAS];
    public static final String RUTA = "src\\palabras.txt";
    
    public static void main(String[] args) {

        String palabraSecreta;
        readFile();
        Scanner input = new Scanner(System.in);

        palabraSecreta = PALABRAS[(int) (Math.random() * NUM_PALABRAS)];

        char[][] caracteresPalabra = new char[2][];
        caracteresPalabra[0] = palabraSecreta.toCharArray();
        caracteresPalabra[1] = new char[caracteresPalabra[0].length];

        String caracteresElegidos = "";
        acerdatoFallido(caracteresPalabra, caracteresElegidos, input, palabraSecreta);

        input.close();
    }

    private static void acerdatoFallido(char[][] caracteresPalabra, String caracteresElegidos, Scanner input, String palabraSecreta) {
        int fallos;
        boolean acertado;
        System.out.println("Acierta la palabra");
        do {

            System.out.println("####################################");

            for (int i = 0; i < caracteresPalabra[0].length; i++) {
                if (caracteresPalabra[1][i] != '1') {
                    System.out.print(" -");
                } else {
                    System.out.print(" " + caracteresPalabra[0][i]);
                }
            }
            System.out.println();

            System.out.println("Introduce una letra o acierta la palabra");
            System.out.println("Caracteres Elegidos: " + caracteresElegidos);
            caracteresElegidos += input.nextLine().toUpperCase();
            fallos = 0;

            fallos = encontrado(caracteresElegidos, caracteresPalabra, fallos);

            paint(fallos);

            acertado = checkWin(fallos, palabraSecreta, caracteresPalabra);

        } while (!acertado && fallos < FALLOS);
    }

    private static boolean checkWin(int fallos, String palabraSecreta, char[][] caracteresPalabra) {
        boolean acertado;
        if (fallos >= FALLOS) {
            System.out.println("Has perdido: " + palabraSecreta);
        }
        acertado = true;
        for (int i = 0; i < caracteresPalabra[1].length; i++) {
            if (caracteresPalabra[1][i] != '1') {
                acertado = false;
                break;
            }
        }
        if (acertado) {
            System.out.println("Has Acertado ");
        }
        return acertado;
    }

    private static int encontrado(String caracteresElegidos, char[][] caracteresPalabra, int fallos) {
        boolean encontrado;
        for (int j = 0; j < caracteresElegidos.length(); j++) {
            encontrado = false;
            for (int i = 0; i < caracteresPalabra[0].length; i++) {
                if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
                    caracteresPalabra[1][i] = '1';
                    encontrado = true;
                }
            }
            if (!encontrado) {
                fallos++;
            }
        }
        return fallos;
    }

    private static void readFile() {
        File fich = new File(RUTA);
        Scanner inputFichero = null;
        
        try {
            inputFichero = new Scanner(fich);
            for (int i = 0; i < NUM_PALABRAS; i++) {
                PALABRAS[i] = inputFichero.nextLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println("Error al abrir fichero: " + e.getMessage());
        } finally {
            if (fich != null && inputFichero != null) {
                inputFichero.close();
            }
        }
    }

    private static void paint(int fallos) {
        switch (fallos) {
            case 1:
                
                System.out.println("     ___");
                break;
            case 2:
                
                System.out.println("      |");
                System.out.println("      |");
                System.out.println("      |");
                System.out.println("     ___");
                break;
            case 3:
                System.out.println("  ____ ");
                System.out.println("      |");
                System.out.println("      |");
                System.out.println("      |");
                System.out.println("     ___");
                break;
            case 4:
                System.out.println("  ____ ");
                System.out.println(" |    |");
                System.out.println("      |");
                System.out.println("      |");
                System.out.println("     ___");
                break;
            case 5:
                System.out.println("  ____ ");
                System.out.println(" |    |");
                System.out.println(" O    |");
                System.out.println("      |");
                System.out.println("     ___");
                break;
            case 6:
                System.out.println("  ____ ");
                System.out.println(" |    |");
                System.out.println(" O    |");
                System.out.println(" T    |");
                System.out.println("     ___");
                break;
            case 7:
                System.out.println("  ____ ");
                System.out.println(" |    |");
                System.out.println(" O    |");
                System.out.println(" T    |");
                System.out.println(" A   ___");
                break;
        }
    }

}
